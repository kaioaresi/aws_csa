## IAM - (Identity and Access Management)

- O AWS Identity and Access Management (IAM) permite que você gerencie com segurança o acesso aos serviços e recursos da AWS. Usando o IAM, você pode criar e gerenciar usuários e grupos da AWS e usar permissões para conceder e negar acesso a recursos da AWS.
- **O IAM é um recurso de sua conta da AWS disponibilizado gratuitamente**. Você será cobrado somente pelo uso de outros serviços da AWS utilizados pelos usuários.
- IAM provê acessos e permissões aos recursos da AWS (como EC2, S3, DynamoDB)
- IAM não substitui seu AD ou outro sistema de gerenciamento de usuários da sua aplicação.
- IAM é um serviço global, suas configurações se aplicam a todas as regiões.

__Utilizamos regurlamente   o IAM para genrenciar:__

- Usuários, grupos, funções do IAM (roles), IAM acess policies, API keys, Políticas de senhas, MFA(multi-factor authenticator) e Rotação de chaves (API Keys).
- Criar e utilizar contas sempre baseadas no principio do **prívilegio minimo**.
- Permite a **Federação de indentidade** (AD, Facebook, Linkedin, Gmail e etc).
- É compliance  aos **PCI DSS**.

### Root user

- Conta criada no momento do cadastro na AWS (e-mail cadastrado)
- O usuário Root tem acesso a todos os recursos e ações na AWS.
- Como boa prática de segurança o usuário Root deve ter configurado o MFA.
- A AWS recomenda __não utilizar o Root user nas tarefas do dia-a-dia, sendo criados Usuário do IAM para administrar os recursos (observando o conceito do privilegio mínimo.)
- Completar os passos de segurança como melhores práticas.


**Criar um novo user para gerenciamento.**


### Policy

- Uma Policy é um documento que declara uma ou mais permissões, em formato json.
- Por padrão, uma policy que nega um recurso/ação *(explicit deny)* sempre sobrescreve outra que permite *(explicit allow)*, negando o acesso ao recurso/ação no caso de cruzamento de permissões.
- O IAM tem várias Policies prontas   como templates, para serem aplicadas a usuários e grupos:
  - **Adminitrator access**: Acesso a todos os recursos da AWS (equivalente ao Root User).
  - **Read only access**: Somente permissão de vizualização dos recursos da AWS.
  - **AmazonS3FullAccess**: Provê o acesso completo a todos os buckets do S3.
- Você pode utilzar templates prontos, customizar a sua policy ou utilizar o Policy generator.
- Mais de uma Policy pode ser aplicada a um usuário/grupo ao mesmo tempo.

**Crie um policy que afete um user(se necessário crie um novo user para aplicar as policy)**

**Observação**: Adicione a policy ao group e valide o usuário.

### API Access key

API Access key são requeridas para as chamadas aos recursos da AWS quando não utilizada a console:

- AWS command line interface (CLI)
- Tools for windows PowerShell
- AWS SDKs
- Chamadas diretass em http as APIs dos recursos da AWS

Fatos importantes sobre a API Access key:

- API keys são geradas e ficam disponíveis somente na criação do usuário, ou quando você solicita a geração de um novo API Access key.
- A AWS não gera novamente o mesmo API Access key.
- As API Access key são sempre associadas a um usuário do IAM.
- Na console que lista os usuários, você só será capaz de ver Access key ID e `nunca o secret ID` (exibido somente na criação ou geração de um novo Access key).
- Roles (Função IAM) não possuem credenciais de API Access key permanentes.
- Se sua API key for descoberta você deve desativá-la imediatamente.
- **Não armazene API keys nas instâncias do EC2**, utilize sempre roles (Função IAM).

__AWS CLI__

```
# Add um API Access Key
aws configure

# Add um API Access Key (perfil específico)
aws configure --profile develop


# Teste - Listando todos buckets
aws s3 ls

# Teste - Lista um bucket específico
aws s3 ls s3://meu_bucket
```

**Crie a api access key em um user e realize alguns comando via CLI**

### Roles

- Uma `role` é uma identidade temporária do IAM criada para permissões especificas, sendo uma forma segura de prover acesso temporário aos recursos da AWS.
- Ao invés de manter credenciais permanentes (API Access key) utilize roles principalmente para recursos da AWS como intâncias EC2.
- Uma role possui uma `politica associada`, a qual irá permitir ou negar o acesso aos recursos da AWS para o usuário ou serviço que assumiu a role.
- Roles podem ser criadas para:
  - EC2
  - Lambda
  - Another AWS Account
  - Web Identity
  - SAML 2.0 federation
  - [ETC](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html)


**Crie um instancia, dps criar um `role` com permissão para view s3 por exemplo e adicione a `role` a instancia e execute alguns comandos cli de dentro da instancia.**

### Security Token Service (STS)

- STS permite a criação temporária de credenciais de segurança, dando permissão a usuários ou recursos executarem ações no ambiente da AWS.
- Essa credencial temporária (token), fica ativa por um curto perído de tempo entre minutos ou algumas horas. Quando expirado o usuário **não tem mais acesso aos recursos**, a menos que tenha sido renovada em tempo.

Quando  requisitar essa credenciais, através de uma chamada de API (STS API Call) são retornados três componentes:
  - Security Token
  - Access key ID
  - Secret access key

Vantans:
  - Identify federation (Feração de indentidade) - você utilizar da sua atual base de usuários, criando uma relação de confiança na qual o STS fornece a credencial temporária para acesso aos recursos, sem a necessidade de criar usuários no IAM. Para a federação com AD utiliza-se o SAML (Security Assertion Markup Language).
  - Web Identify Federation (integração com a autenticação do facebook, Google, Amazon e etc). Suporte ao OpenID Connect (OIDC) - compativel com IdP
  - Uso nas roles aplicadas aos recursos (EC2, Lambda e etc.) e em Cross-account Access.

---
### Tipos de Políticas

- **Identify-based policies** são atachadas a um usuário do IAM, Groupo ou role. Definie o que aquele principal pode executar.
- **Resource-based policies** são utilizadas em recursos, de forma inline. Define quem pode acessar aquele recurso e o que pode executar. Serviçõs como o S3, AWS KMS utilizam políticas baseadas em recursos.

![Identity-based policies and resource-based policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/images/Types_of_Permissions.diagram.png)

- **Permissions boundary** podem para controlar as permissões máximas que um usuário ou role pode ter, evitando escalonamento de privilegio ou permissões muito extensivas.

Basicamente limita os serviços ou ações, mesmo que haja uma política permitindo

Casos de uso:
  - Pode ser utilizada para delegar tarefas de gerenciamento, como criação de usuário
  - Um desenvolvedor assumir uma role destinada a um lambda com permissões mais restritas

![Identity-based policies with boundaries](https://docs.aws.amazon.com/IAM/latest/UserGuide/images/permissions_boundary.png)

![](https://docs.aws.amazon.com/IAM/latest/UserGuide/images/EffectivePermissions-rbp-boundary-id.png)

> Basicamente sobrepoem as permissões do usuário, mesmo se o mesmo for admin de algum recurso, vpcê pode limitar até que ponto do recursos ele pode acessar, exemplo é um usuário que tem permissão admin em IAM, ai você quer que esse usuário tenha apenas permissão para criar usuários dentro do IAM, você pode criar uma `policy` com essas permissões e posteriormente editar o usuário e  adiciona uma `Permissions boundary` com a policy criada.

---

# Labs

**Lab 1 - Cross Account**
- Crie um nova conta
- Crie um truste entre ambas contas
- Crie um role com algumas permissões ao um usuário (readOnly poir é apenas teste)
- Realize o loging com o usuário e realizar o cross account.
- Valide se suas permissões estão corretas.


**Lab 1 - done**

1 - Vou nomear as contas como conta MG e conta SP, para facilitar a explicação. Na conta de MG é onde eu tenho meu usuários, já na conta SP eu criei uma role, do tipo `another account` informando o ID da conta de MG, nessa role defini as policies da minha escolha, no caso read s3 apenas;
2 - Na conta MG realizei o login e na opção `swith roles` coloquei o nome da role criada na conta SP e o ID da conta de SP.
3 - Após isso jã consegui realizar o login na conta SP partindo da conta de MG, =)

**Lab 2 - Role resource**

- Crie um instancia EC2 básica
- Acesse a instancia via ssh e realize os seguintes comandos `aws s3 list`, ira lhe retornor uma mensagem de erro.
- Agora vamos criar um `role` com a permissão de apenas leitura do s3.
- Com a role criada edite a ec2 clicando em Actions >> Security >> Modify IAM role e adicione a role criada anteriormente.
- Realize o login na instancia novamente e realize novamente o `aws s3 list`, ele listará todos buckets criado, caso não verifique as permissões da role.
