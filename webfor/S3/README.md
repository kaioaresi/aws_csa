# S3 (Simple Storage Service)

S3 (Simple Storage Service) é um serviço de armazenamento (storage) através de interface web. Com caracteristicas de durabilidade, tolerância a falhas, segurança e alta  escalabilidade, integrado a diversos outros serviços da AWS que requerem o armazenamento de dados.

armazenamento ilimitado de objetos, com escalabilidade automática.

Casos comuns de uso para s3 incluem:

- Backup a archive para dados on-premises ou da nuvem
- Distribuição de conteúdo como vídeos, fotos, software e mídia em geral
- Armazenamento de dados de Big Data
- Soluções de Disaster Recovery (DR)
- Hospedagem de sites Estáticos

## Bucket

Considere o __bucket__ como um container para armazenar objetos

- Nome de buckets são globais, ou seja, únicos em todo o ambiente da AWS
- Cada conta por padrão pode ter até 100 buckets
- Buckets não podem ser criados dentro de buckets, mas, podem conter sub name spaces (similar a uma pasta)
- Buckets não podem ser transferidos de proprietários
- São permitidos o uso de tags para organizar os buckets
- Buckets são __flat__ (estrutur de arquivos únicos, não hieráquica)
- Somente buckets vazios podem ser excluídos

### Objeto

Objetos são arquivos armazenados em um bucket do S3

- Objetos pode ter de 0 bytes até 5TB
- Cada objeto do AWS S3 tem dados, um chave e metadado. Data, Key name e metadata
- A chave de objeto (ou nome da chave) identifica unicamente o objeto em um bucket
- System metadata (data de modificação, tamanho do objeto, MD5 digest, HTTPS Content-type, Storage Type, etc.)
- User metadata (opcional) - Definido na criação do objeto, podem ser utilizadas tags customizadas como atributos para fins de gerenciamento
- cada objeto pode ser acessado por um url única:

### Tipos de storage

- __Block store__: Opera em um nível mais baixo (raw storage devide leval), gerenciando os dados em blocos de tamanho fixos pré-definidos. Utilizados por exemplo, para a instalação de um sistema operacional de um servidor, como um disco apresentado geralmente através de um SAN (Storage area network)

- __File storage__: Opera em um nível mais alto (system operacional leval) - gerenciando os dados através de hieráquia e nomeclatura de pastas e arquivos. É uso de protocolos como o NFS (Network File System), SMB (Server Message Block) e CIFS (Common Internet File System) atachado diretamente ou através da rede.

- __Object Storage (S3)__: Existe a abstração dos sistem operacional / software. Não é utilizado o SCSI, CIFS ou NGS e sim API (Application Program Interface) sobre o padrão HTTP.

![Type storage](../img/type_storage.svg)

### Prefix e Delimiter

Buckets possuem uma estrutura flat, mas, podem ter uma representação visual de estrutura hierárquica, permitindo a organização por pastas.

### Permissões

Todos os buckets e objetos são **privados por padrão**. Somente o proprietário (owner) tem acesso.

__Identity-based policies__
  - São políticas atachadas a usuários do IAM, Grupos ou Roles

__Bucket policy__
  - É uma política atachada diretamente ao bucket do S3, valendo para todos os objetos
  - Permite acesso público ao bucket, forçar objetos a serem criptografados, garantir acesso a outra conta (cross account), permitir CORS (Cross-origin resource sharing)

```
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"PublicRead",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject","s3:GetObjectVersion"],
      "Resource":["arn:aws:s3:::DOC-EXAMPLE-BUCKET/*"]
    }
  ]
}
```

> Adicionando essa policy, todos objetos dentro do bucket poderão ser acessados por qualquer pessoa.

__S3 access Control Lists__
  - Permite o acesso de outros usuários da AWS ou acesso público ao nível de objeto.

>> Existe um configuração que possibilita o bloqueio em massa de todos buckets, S3 >> Account settings for Block Public Access > Edit > Block all public access

__S3 Access Point__
  - Cria pontos de acesso dando a aplicações ou usuários diferentes permissões com base em cada endpoint (permissões por VPC ID, Tags e Prefixos de Objetos)

![S3 Access point](https://d1.awsstatic.com/re19/Westeros/Diagram_S3_Access_Points.fa88c474dc1073aede962aaf3a6af2d6b02be933.png)

### Storage Classes


__Standard__

- Desenhada para propósito geral, sendo a opção default;
- É a classe de storage ideial para todos que requeiram alta performance, alta durabilidade e que são acessados frequentemente. Considere dados de missão crítica;
- Durabilidade de `99,999999999%` . Armazena os dados em 3 ou mais AZs;
- Disponibilidade de `99,99%`.
- **É a classe de storage mais cara.**
- [Preço storage](https://aws.amazon.com/pt/s3/pricing/)

__Standard-infreguent Access (S3-IA)__

- Desenhada para dados de longa duração que são pouco acessados, mas, que precisam estar disponíveis em `tempo real` quando requerido o acesso.
- Utilizada por exemplo, para documentos mais antigos. Ou ainda para o armazenamento de backups;
- Os dados nessa categoria geralmente permanecem por no mínimo 30 dias;
- **Mais econômicos que standard e RRS (para todos poucos acessados)**

__One zone-infreguent Access (On Zone-IA)__

- Dados acessados com pouca frequência que não requerem da disponibilidade e da resiliência dos armazenamentos S3 standard ou S3 standard - IA
- Classe de dados recomendada para armazenar cópias secundárias de backup, dados que podem ser recriados como facilidade e são pouco acessados, bem como para armazenamento usado como destino de um replicação entre regiões do S3
- Menos durabilidade, armazenamento os dados em somente uma Zona de disponibilidade
- Disponibilidade de `99,5%`
- Custa `20%` menos que armazenamento na categoria `S3 Standard - IA`

__Reduced Redundancy Storage (RRS)__

- Desenhada para dados não-crítios, que podem ser reproduzidos
- Classe de dados recomendada para dados que podem ser recriados, caso sejam perdidos. Por exemplo, a geração de thumbnails à partir da imagem original. Vídeos codificados em diferentes formatos.
- **NÃO RECOMENDADO**

__Intelligent Tiering__

A classe de armazenamento `S3 Intelligent-Tiering` foi projetada para otimizar os custos movendo `automaticamente` os dados para o nível de acesso mais econômico, sem impacto na performance ou sobrecarga operacional.

- O Amazon S3 monitora os padrões de acesso dos objetos no  S3 Intelligent-Tiering  e move aqueles que não foram acessados por 30 dias consecutivos para o nível de acesso infrequente S3-IA. Ou retornado ao S3 Standard quando os dados passam a ser acessados frequentemente.
- É a classe de armazenamento ideal para dados duradouras como padrões de acesso desconhecidos ou imprevisíveis.

#### Glacier

O amazon **Glacier** é um serviço de storage que oferece armazenamento seguro, durável e com um custo extremamente baixo para dados que geralmente não são requeridos em tempo real como archives e long-term backups.

- Foi projetado para dados que não são acessados frequentemente, onde um tempo de recuperação/disponibilização dos dados na ordem de minutos a algumas horas é aceitável;
- É integrado ao S3 sendo aprensentado como um classe de armazenamento, comumente utilizado para dados que precisam ser armazenados por anos. Alternativa aos backups em fita realizados no ambiente on-premises;
- Cada arquivo no Glacier é chamado de archive estando contido em um Vault;
- O vault do Glacier é **criptografado** por padrão. A feature Vault lock bloqueia qualquer alteração, garantido compliance e requisitos regulatórios.

##### Glacier - Classes e Recuperação

**Amazon Glacier** - Opções de recuperação (retrieval options):

- Expedited (1 to 5 minutes)
- Standard (3 to 5 hours)
- Bulk (5 to 12 hours)

__Armazenamento por no mínimo 90 dias__

**Amazon Glacier Deep Archive** - Para armazenamentos de longa data (long term storage) super econômico:

- Standard (12 hours)
- Bulk (48 hours)

__Armazenamento por no mínimo 180 dias__

---

## Lifecycle Policy

A política de ciclo de vida determina regras e a transição automática das classes de storage aos objetos do bucket. Baseada em intervalos de tempo.

Deve ser customizada de acordo com a política de retenção da empresa

Ajuda a reduzir substancialmente o custo do armazenamento de dados

Pode ser aplicada a todos os objetos do bucket, ou aos objetos com um prefixo em específico. Uso de exemplo:

1. Armazenamento dos arquivos no S3 Standard
2. Depois de 30 dias, migrar para standard-IA
3. Depois de 90 dias, migrar para o Amazon Glacier
4. Depois de 03 anos, deletar os objetos

![Lifecycle Policy](../img/S3_LifeCycle.jpg "Lifecycle Policy")

## Versionamento

A feature de versioning ajuda a proteger arquivos contra a deleção acidental ou maliciosa, mantendo multiplas cópias das versões do arquivo no bucket

- É habilitada no nível de bucket, valendo para todos os objetos
- Objetos com a mesma `key` são sobrescritos, sendo salvos com múltiplas versões cada um com um `Version ID` distinto
- Antes da habilitação da feature todos os objetos possuem version id null
- Utilizando o versioning e lifecycle policy é possivel criar uma solução robusta de backup e archiving no S3

> Para habilitar o versioning: Clique no bucket >> Properties >> Bucket Versioning >> Edit >> Bucket Versioning `Enable` >> save

## Event notifications

Eventos podem ser acionados à partir de ações nos objetos armazenados no S3, por exemplo de gravação, exclusão cópia ou perda de um objeto. Exemplo de uso do S3 events:

- Realizar a codificação de um arquivo de vídeo
- Recriar um objeto quando  for perdido (classe RRS)
- Processar um arquivo de dados quando ele for salvo no s3

A notificação de eventos pode acionar os seguintes servições:

- AWS Lambda
- Amazon SNS
- Amazon SQS

## Hosting a static website on Amazon S3

Os serviços do S3 suporta a hospedagem de sistes estáticos (Static website hosting)
Sem processamento do lado do servidor (não suporta PHP, ASP .net)

Hospedando um site estático no S3 você tem as seguintes vantagens:

- Alta performance
- Escalabilidade automática
- Serveless (sem administração de servidores)
- Alta durabilidade e disponibilidade
- Segurança (DDoS, Patches, Disaster Recovery, etc.)
- Baixo custo

## Server Access Logging

O Server Access Logging permite a rastreabilidade dos acessos ao Bucket do S3, gerando logs salvos no próprio bucket ou em outro que você indicar

O logging vem desabilitado por padrão, uma vez habilitado todos os acessos serão registrados, estando disponíveis em alguns minutos

Feature bastante útil para atender requisitos de conformidade e auditoria

As informações dos log incluem:

- Conta AWS de Requisitante e Endereço IP
- Nome do bucket
- Data/Hora da requisição
- Ação (GET, PUT, LIST, entre outras)
- Resposta do S3 ou código de erro

## Multipart Upload Overview

Single operation upload

- É o envio tradicional, permite arquivos de até 5GB, mas a aws recomenda o uso do `multipart upload` para arquivos acima de 100MB

Multipart Upload

- Permite o envio de mútiplas partes do arquivo simultaneamente (paralelismo)
- Permite o pause/resume dos arquivos em upload
- Se uma das partes falhar, você envia somente essa parte (evitando retrasmissão completa)
- Quando todas as partes forem transmitas o S3 reune as partes, restaurando seu arquivo original
- Uso obrigatório para arquivos maiores que 5GB

## Transfer Acceleration

O amazon s3 transfer acceletation possibilita transferências de arquivos rápidos, fáceis e seguras em longas distâncias entre o seu cliente e um bucket do S3

O transfer acceleration aproveita os pontos de presença distribuídos globalmente do amazon cloudfront. Conforme os dados chegam em um ponto de presença, eles são roteados para o amazon S3 através da rede interna da AWS, mais rápida e otimizada

