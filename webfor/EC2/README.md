# EC2 (Elastic Compute Cloud)

- Amazon EC2 são servidores vituais (instâncias) que provêm grande flexibilidade e escalabilidade. Diferentes tipos e tamanhos de instâncias estão disponíveis aos clientes.
- `AMI` (amazon machine image) - image como o SO, software e outros configurações.
- `Instance Type` - os recursos de hardware (processador, memoria, I/O para disco, rede e etc.)
- `Network interface` - (privada, pública ou elastic IP addresses)

Storage - disco da instância:

- `EBS` (Elastic block storage) - Network storage persistente
- `Instance store` - storage local efêmero
- `EFS` (Elastic file system) - fornece um sistema de arquivos NFS elástico

Tipos de instances

Exitem vários tipos de instances, e cada instance pertensem a uma familia, e cada familia tem um proposito de uso, segue alguns exemplos:

- __Uso geral:__ Instâncias de uso geral fornecem um equilíbrio de recursos de computação, memória e rede e podem ser usadas para diversas cargas de trabalho. Essas instâncias são ideais para aplicativos que usam esses recursos em proporções iguais, como servidores da Web e repositórios de código, ex: A1, T4g, T3, T2, M6g, M5, M5a, M5n, M4.
- __Otimizadas para computação:__ As instâncias otimizadas para computação são ideais para aplicativos vinculados a computação que se beneficiam de processadores de alto desempenho. As instâncias pertencentes a essa família são adequadas para cargas de trabalho de processamento em lote, transcodificação de mídia, servidores da Web de alto desempenho, computação de alto desempenho (HPC), modelagem científica, servidores de jogos dedicados e mecanismos de servidor de anúncios, inferência de machine learning e outros aplicativos de uso intensivo de computação, ex: C6g, C5, C5a, C5n, C4.
- __Otimizadas para memória:__ As instâncias otimizadas de memória são projetadas para fornecer desempenho rápido para cargas de trabalho que processam grandes conjuntos de dados na memória, ex: R6g, R5, R5a, R5n, R4, X1e, X1, z1d.
- Computação acelerada: Instâncias de computação aceleradas usam aceleradores de hardware, ou coprocessadores, para executar funções, como cálculos de número de ponto flutuante, processamento de gráficos ou correspondência de padrões de dados, mais eficientemente do que é possível no software em execução nas CPUs, ex: P4, P3, P2, Inf1, G4, G3, F1.
- __Otimizadas para armazenamento:__ As instâncias otimizadas para armazenamento são projetadas para cargas de trabalho que exigem acesso de leitura e gravação sequencial alto a conjuntos de dados muito grandes no armazenamento local. Elas são otimizadas para fornecer dezenas de milhares de operações de E/S aleatórias de baixa latência por segundo (IOPS) para aplicativos, ex: I3, I3en, D2, H1.

__Elastic IP__

Permite que seja alocado um IP publico fixo em uma instancia.
