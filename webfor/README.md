# AWS - Certified Solutions Architect - Associate

## Introdução

## O que é computação em nuvem ?

A computação em nuvem é a entrega de recursos de TI sob demanda por meio da Internet com definição de preço de pagamento conforme o uso. Em vez de comprar, ter e manter datacenters e servidores físicos, você pode acessar serviços de tecnologia, como capacidade computacional, armazenamento e bancos de dados, conforme a necessidade, usando um provedor de nuvem.

## Tipos de computação em nuvem ?

Exitem basicamente três tipo de computação em nuvem

- Infraestrutura como um serviço (IaaS)
- Plataforma como um serviço (PaaS)
- Software como um serviço (SaaS)

### Infraestrutura como um serviço (IaaS)

A Infraestrutura como um serviço, às vezes abreviada como IaaS, contém os componentes básicos da TI em nuvem e, geralmente, dá acesso (virtual ou no hardware dedicado) a recursos de rede e computadores, como também espaço para o armazenamento de dados. A Infraestrutura como um serviço oferece a você o mais alto nível de flexibilidade e controle de gerenciamento sobre os seus recursos de TI e se assemelha bastante aos recursos de TI atuais com os quais muitos departamentos de TI e desenvolvedores estão familiarizados hoje em dia.

### Plataforma como um serviço (PaaS)

Com a Plataforma como um serviço, as empresas não precisam mais gerenciar a infraestrutura subjacente (geralmente, hardware e sistemas operacionais), permitindo que você se concentre na implantação e no gerenciamento das suas aplicações. Isso o ajuda a tornar-se mais eficiente, pois elimina as suas preocupações com aquisição de recursos, planejamento de capacidade, manutenção de software, correção ou qualquer outro tipo de trabalho pesado semelhante envolvido na execução da sua aplicação.

### Software como um serviço (SaaS)

O Software como um serviço oferece um produto completo, executado e gerenciado pelo provedor de serviços. Na maioria dos casos, as pessoas que se referem ao Software como um serviço estão se referindo às aplicações de usuário final. Com uma oferta de SaaS, não é necessário em como o serviço é mantido ou como a infraestrutura subjacente é gerenciada, você só precisa pensar em como usará este tipo específico de software. Um exemplo comum de aplicação do SaaS é o webmail, no qual você pode enviar e receber e-mails sem precisar gerenciar recursos adicionais para o produto de e-mail ou manter os servidores e sistemas operacionais no qual o programa de e-mail está sendo executado.


## Infraestrutura global

Infraestrutura global é o que a AWS chama de composição de regiões, zonas de disponibilidades e os pontos de presença.

![AZ](img/az.png)

> https://infrastructure.aws/

### Região

É uma área geográfica definida, sendo completamente independente. Possui duas ou mais Zonas de Disponibilidade.

### Zonas de Disponibilidade

São Datacenters onde os recursos são armazenados e processados, sendo isoladas entre si, todavia, interligados por links de alta velocidade e baixa latência.

### Pontos de Presença

São Datacenters mais enxutos da AWS espalhados pelo mundo, os quais oferecem serviços como CDN e DNS com baixa latência, melhorando a experiência dos usuários.

---
- Intrudução a cloud computing
- IAM (Identity and Access Management)
- S3 (Simple Storage Service)
- EC2 (Elastic Compute Cloud)

---

# Referências

- [What is cloud computing](https://aws.amazon.com/pt/what-is-cloud-computing/)
- [Types of cloud computing](https://aws.amazon.com/pt/types-of-cloud-computing/)
- [Global infrastructure](https://aws.amazon.com/pt/about-aws/global-infrastructure/)
- [Infrastructure AWS](https://infrastructure.aws/)
- [AWS free tier](https://aws.amazon.com/pt/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc)
- [AWS cost explorer](https://aws.amazon.com/pt/aws-cost-management/aws-cost-explorer/)
- [IAM](https://aws.amazon.com/pt/iam/)
- [IAM - Access Policies](https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/access_policies.html)
- [IAM - AWS arns and namespaces](https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html)
- [IAM - AWS Access key](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)
- [IAM - Reference aws services that work with iam](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html)
- [IAM - AWS Security Token Service](https://docs.aws.amazon.com/en_pv/STS/latest/APIReference/Welcome.html)
- [Ferramentas para desenvolver e gerenciar aplicativos na AWS](https://aws.amazon.com/pt/tools/)
- [Temporary Security Credentials](https://docs.aws.amazon.com/en_pv/IAM/latest/UserGuide/id_credentials_temp.html)
- [IAM - Using Service-Linked Roles](https://docs.aws.amazon.com/en_pv/IAM/latest/UserGuide/using-service-linked-roles.html)
- [IAM - Cross-Account-EC2-Read (IAM Policy) – Lab](http://webfor.com.br/wp-content/uploads/2019/09/Cross-Account-EC2-Read.txt)
- [IAM - Identity-Based Policies and Resource-Based Policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_identity-vs-resource.html)
- [IAM - Permissions Boundaries for IAM Entities](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_boundaries.html)
- [S3](https://aws.amazon.com/pt/s3/)
- [S3 - Prefix Delimiter](https://docs.aws.amazon.com/pt_br/AmazonS3/latest/dev/ListingKeysHierarchy.html)
- [S3 - Storage class intro ](https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html)
- [S3 - Setting bucket and object access permissions](https://docs.aws.amazon.com/AmazonS3/latest/user-guide/set-permissions.html)
- [S3 - Bucket Policy Examples](https://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html)
- [S3 - Bucket Policy Examples 2](https://docs.aws.amazon.com/pt_br/AmazonS3/latest/dev/example-policies-s3.html)
- [S3 - Amazon S3 Access Points](https://aws.amazon.com/pt/s3/features/access-points/?nc1=h_ls)
- [S3 - Storage class](https://docs.aws.amazon.com/pt_br/AmazonS3/latest/dev/storage-class-intro.html)
- [S3 - Storage price](https://aws.amazon.com/pt/s3/pricing/)
- [S3 - Amazon S3 Glacier](https://aws.amazon.com/pt/glacier/?nc1=h_ls)
- [S3 - Comparing the Amazon S3 storage classes](https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html#sc-compare)
- [S3 - Lifecycle Policy](https://docs.aws.amazon.com/pt_br/AmazonS3/latest/dev/object-lifecycle-mgmt.html)
- [S3 - Configuring Amazon S3 event notifications](https://docs.aws.amazon.com/AmazonS3/latest/dev/NotificationHowTo.html)
- [S3 - Hosting a static website on Amazon S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)
- [S3 - Amazon S3 server access logging](https://docs.aws.amazon.com/AmazonS3/latest/userguide/ServerLogs.html)
- [S3 - Adding a Bucket Policy to Require MFA](https://docs.aws.amazon.com/AmazonS3/latest/userguide/example-bucket-policies.html#example-bucket-policies-use-case-7)
- [S3 - Multipart Upload](https://docs.aws.amazon.com/AmazonS3/latest/dev/mpuoverview.html)
- [S3 - Amazon S3 Transfer Acceleration](https://docs.aws.amazon.com/pt_br/AmazonS3/latest/userguide/transfer-acceleration.html)
- [EC2](https://aws.amazon.com/pt/ec2/)
- [EC2 - instance connect methods](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-connect-methods.html)
- [EC2 - instance tipe](https://aws.amazon.com/pt/ec2/instance-types/?nc1=h_ls)
- [EC2 - Elastic ip addresses eip](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html)
